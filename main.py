import psycopg2
import psycopg2.extras
import json
from flask import Flask, render_template
app = Flask(__name__, static_url_path='')

@app.route('/api')
def data():
    connection = psycopg2.connect(host="localhost", database="meinedb", user="postgres", password="")
    cur = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute('SELECT * FROM meinedaten')
    data = cur.fetchall()
    cur.close()
    connection.close()
    return json.dumps([dict(x) for x in data])

@app.route('/')
def index():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8081, debug=True)